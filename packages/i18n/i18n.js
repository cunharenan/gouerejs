i18n = {

	languages: [],

	t: function (str) {
		var lang = Session.get('language') || 'pt_BR';
		if(this.languages[lang] && this.languages[lang][str]) {
			return this.languages[lang][str];
		}
		return str;
	},

	tc: function (index) {
		var lang = Session.get('language') || 'pt_BR';
		var indexes = index.split('.');
		if(this.languages[lang] && this.languages[lang]['complex']) {
			var ret;
			for (var i = 0; i < indexes.length; i++) {
				if(!ret) {
					ret = this.languages[lang]['complex'][indexes[0]];
				}
				else {
					ret = ret[indexes[i]];
				}
			};
			return ret;
		}
		return undefined;
	}

}

if(Meteor.isClient){
	Handlebars.registerHelper('i18n', function(str){
		return i18n.t(str);
	}); 
}