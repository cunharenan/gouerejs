
Meteor.publish('settings', function() {
  var options = {};
  if(!isAdminById(this.userId)){
    options = _.extend(options, {
      fields: {
      }
    });
  }
  return Settings.find({}, options);
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Cities -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
// Publish all Cities
Meteor.publish('allCities', function() {
  return Cities.find({});
});

// Publish cities by state
Meteor.publish('citiesByState', function(stateId) {
  return Cities.find({state_id: stateId});
});

// Publish single city
Meteor.publish('singleCity', function(cityId) {
  return Cities.findOne(cityId);
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Users --------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
// Publish the current user
Meteor.publish('currentUser', function() {
  var user = Meteor.users.find(this.userId);
  return user;
});

// Publish all users
Meteor.publish('allUsers', function () {

  if(!isAdminById(this.userId))
    return [];

  return Meteor.users.find({});
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Places -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
// Publish all Places
Meteor.publish('allPlaces', function() {
  return Places.find({});
});

Meteor.publish('filteredPlaces', function (q) {
  var find = getPlacesQueryParams(q);

  if(!q)
    return [];

  if(!q.city_id && !q.ids)
    return [];

  var options = {
    sort: {
      start_date: 1
    }
  };
  var events = Places.find(find, options);
  return events;
});

//Publish single Place
Meteor.publish('singlePlace', function(id) {
  return Places.findOne(id);
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Events -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Meteor.publish('allEvents', function () {

  if(!isAdminById(this.userId))
    return [];

  return Events.find({});
});

Meteor.publish('filteredEvents', function (q) {
  var find = getEventsQueryParams(q);

  if(!q || !q.city_id)
    return [];

  var options = {
    sort: {
      start_date: 1
    }
  };
  var events = Events.find(find, options);
  return events;
});

Meteor.publish('singleEvent', function (id) {
  var evt = Events.find({_id: id});
  var place = Places.find({_id: evt.fetch()[0].place_id});
  return [evt, place];
});

Meteor.publish('eventPictureById', function (id) {
  var evt = Events.findOne({_id: id});
  var evtFS = EventsFS.find({_id: evt.picture_id});
  return evtFS;
});

Meteor.publish('eventPlaceById', function (id) {
  var evt = Events.findOne({_id: id});
  var place = Places.find({_id: evt.place_id});
  return place;
});

Meteor.publish('eventCityById', function (id) {
  var evt = Events.findOne({_id: id});
  var place = Places.find({_id: evt.place_id});
  var city = Cities.find({_id: place.city_id});
  return city;
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Search -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Meteor.publish('search', function (q) {
  var events = Events.find({name: {$regex: q, $options: 'i'}});
  var events_fetch = events.fetch();

  var pictures_id = [];
  _.each(events_fetch, function (e) {
    if(e.picture_id) {
      pictures_id.push(e.picture_id);
    }
  });
  var pictures = EventsFS.find({_id: {$in: pictures_id}});

  var places_ids = [];
  _.each(events_fetch, function (e) {
    places_ids.push(e.place_id);
  });
  var places = Places.find({$or: [{name: {$regex: q, $options: 'i'}}, {_id: {$in: places_ids}}]});
  var places_fetch = places.fetch();

  var cities_ids = [];
  _.each(places_fetch, function (p) {
    cities_ids.push(p.city_id);
  });
  var cities = Cities.find({$or: [{name: {$regex: q, $options: 'i'}}, {_id: {$in: cities_ids}}]});
  
  return [events, places, cities, pictures];
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Events Logs --------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Meteor.publish('eventsLogsById', function (id) {
  var events_logs = EventsLogs.find({'data.event_id': id}, {limit: 20, sort: { 'data.votedAt': -1 }});
  var events_logs_fetch = events_logs.fetch();

  var users_ids = [];
  _.each(events_logs_fetch, function (l) {
    users_ids.push(l.data.user_id);
  });
  var users = Meteor.users.find({_id: {$in: users_ids}});

  return [events_logs, users];
});





