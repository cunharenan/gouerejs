getEventsQueryParams = function (q) {
  var find = {};
  var places_ids = [];

  if(!q || !q.city_id)
    return {};

  var places = Places.find({city_id: q.city_id});
  places_ids = _.pluck(places.fetch(), '_id');

  //mandatory => filter for places
  //this is to avoid big data
  _.extend(find, {place_id: {$in: places_ids}});


  if(q.start_date && q.end_date) {
    //setup dates to cover the entire day
    var start_date = moment.unix(q.start_date).hour(0).minute(0).second(0).unix();
    var end_date = moment.unix(q.end_date).hour(23).minute(59).second(59).unix();
    _.extend(find, {
      start_date: {
        $gte: start_date,
        $lt: end_date
      }
    })
  }

  //regex search on name
  if(q.name) {
    _.extend(find, {
      name: {$regex: q.name, $options: 'i'}
    });
  }

  //filters of attractions
  if(q.attractions) {

    var orArray = [];

    if(q.attractions.edm) {
      orArray.push({ 'attractions.edm': true });
    }

    if(q.attractions.sertanejo) {
      orArray.push({ 'attractions.sertanejo': true });
    }

    if(q.attractions.pagode) {
      orArray.push({ 'attractions.pagode': true });
    }
    
    if(q.attractions.hip_hop) {
      orArray.push({ 'attractions.hip_hop': true });
    }

    if(q.attractions.rock) {
      orArray.push({ 'attractions.rock': true });
    }

    if(q.attractions.funk) {
      orArray.push({ 'attractions.funk': true });
    }

    if(orArray.length > 0) {
      _.extend(find, {
        $or: orArray
      });
    }
  }

  //status
  if(q.status) {
    _.extend(find, {
      status: q.status
    })
  }

  return find;
}