Template.admin_places_new.rendered = function () {
  var self = this;

  //--- init map ---//
  $.proxy(Template.admin_places_new.initMap, this)();

  //--- autocomplete ---//
  $(this.find('[name="city"]')).autocomplete({
    source: function( request, response ) {
      Meteor.call('cities_list', request.term, function (err, res) {
        response( $.map( res, function( item ) {
          var show = item.name;
          return {
            label: show,
            value: show,
            id: item._id,
            latitude: item.latitude,
            longitude: item.longitude
          }
        }));
      });
    },
    change: function (event, ui) {
      $(self.find('[name="city_id"]')).val(ui.item.id);
      if(ui.item.latitude && ui.item.longitude) {
        console.log(ui.item);
        Template.admin_places_new.map.setView([ui.item.latitude, ui.item.longitude], 13);
      }
    }
  });
}

Template.admin_places_new.events({
  'submit form': function (e) {
    e.preventDefault();
    var form = $(e.currentTarget);
    var dataArray = form.serializeArray();
    var data = {};
    _.each(dataArray, function (o) {
      if(o.value && o.value !== '')
        data[o.name] = o.value;
    });

    Meteor.call('places_new', data, function (error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
      else {
        Router.go('/admin_index');
      }
    });
  },
});

Template.admin_places_new.initMap = function () {
  var map_id = 'map-box',
    self = this,
    container = L.DomUtil.get(map_id),
    loaded = (container !== undefined && container._leaflet);

  if(!loaded) { 
    var map = L.map('map-box').setView([-28.9333, -49.4833], 13);   
    L.tileLayer('http://{s}.maptile.lbs.ovi.com/maptiler/v2/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?token={devID}&app_id={appID}', {
        attribution: 'Map &copy; <a href="http://developer.here.com">Nokia</a>, Data &copy; NAVTEQ 2012',
        subdomains: '1234',
        devID: 'xyz',
        appID: 'abc'
    }).addTo(map);
    Template.admin_places_new.map = map;

    Template.admin_places_new.map.on('click', function (e) {
      if(!Template.admin_places_new.marker) {
        Template.admin_places_new.marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(Template.admin_places_new.map);
      }
      else {
        Template.admin_places_new.marker.setLatLng([e.latlng.lat, e.latlng.lng]);
      }

      $(self.find('[name="latitude"]')).val(e.latlng.lat);
      $(self.find('[name="longitude"]')).val(e.latlng.lng);
    });
  }
}