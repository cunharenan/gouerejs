Places = new Meteor.Collection('places');

Places.allow({
	insert: isAdminById,
	update: isAdminById,
	remove: isAdminById
});

Meteor.methods({

  //Place list for autocomplete
  places_list: function (term) {
    var places = Places.find({name: { $regex : term, $options: 'i' }}, {limit: 10}).fetch();
    var cities_ids = _.pluck(places, 'city_id');
    var cities = Cities.find({_id: {$in: cities_ids}}).fetch();

    _.each(places, function (place, index) {
      var city = _.find(cities, function (c) {
        return c._id == place.city_id;
      });
      place.city = city;
    }); 

    return places;
  },

  //insert new place
  places_new: function (data) {
    var name = data.name,
      latitude = data.latitude,
      longitude = data.longitude,
      city_id = data.city_id,
      description = data.description,
      user = Meteor.user();

    if(!user || !user.isAdmin)
      throw new Meteor.Error(601, 'Para criar um novo evento é preciso ser um administrador!');

    if(!name || !latitude || !longitude || !city_id)
      throw new Meteor.Error(409, 'É preciso preencher todos os campos!');

    var place = {
      name: name,
      latitude: latitude,
      longitude: longitude,
      city_id: city_id,
      description: description,
      author_id: user._id,
      creation_date: moment().unix(),
      status: PLACES_STATUS.enabled
    }

    var place_id = Places.insert(place);

    return place_id;
  },

  //edit places
  places_edit: function (id, data) {
    var name = data.name,
      latitude = data.latitude,
      longitude = data.longitude,
      city_id = data.city_id,
      description = data.description,
      user = Meteor.user();

    if(!user || !user.isAdmin)
      throw new Meteor.Error(601, 'Para criar um novo evento é preciso ser um administrador!');

    if(!name || !latitude || !longitude || !city_id)
      throw new Meteor.Error(409, 'É preciso preencher todos os campos!');

    var place_id = Places.update({_id: id}, {
      $set: {
        name: name,
        latitude: latitude,
        longitude: longitude,
        city_id: city_id,
        description: description,
        last_change: moment().unix()
      }
    });

    return place_id;
  },

  //enable an place
  'places_enable': function (place_id) {
    var user = Meteor.user(),
      place = Places.findOne(place_id);

    if(!user || !user.isAdmin)
      throw new Meteor.Error(601, 'Para alterar o status de um evento é preciso ser um administrador!');

    if(!place)
      throw new Meteor.Error(602, 'Não foi encontrado nenhum local com o código ' + place_id);

    if(place.status == EVENTS_STATUS.enabled)
      throw new Meteor.Error(601, 'O local ' + place.name + ' já se encontra ativo!');

    Places.update({_id: place_id}, {$set: {status: PLACES_STATUS.enabled}});

    return place_id;
  },

  //disable an event
  'places_disable': function (place_id) {
    var user = Meteor.user(),
      place = Places.findOne(place_id);

    if(!user || !user.isAdmin)
      throw new Meteor.Error(601, 'Para alterar o status de um evento é preciso ser um administrador!');

    if(!place)
      throw new Meteor.Error(602, 'Não foi encontrado nenhum local com o código ' + place_id);

    if(place.status == EVENTS_STATUS.disabled)
      throw new Meteor.Error(601, 'O local ' + place.name + ' já se encontra desabilitado!');

    Places.update({_id: place_id}, {$set: {status: PLACES_STATUS.disabled}});

    return place_id;
  }
});

PLACES_STATUS = {
  disabled: 0,
  enabled: 1
}