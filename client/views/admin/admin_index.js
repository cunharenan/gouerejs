Template.admin_index.helpers({
  countEvents: function () {
    return Events.find({}).count();
  },
  countUsers: function () {
    return Meteor.users.find({}).count();
  },
  countPlaces: function () {
    return Places.find({}).count();
  },
  countCities: function () {
    return Cities.find({}).count();
  },
  usersToday: function () {
    var today = moment(moment().format('DD/MM/YYYY'));
    var endToday = moment(moment().format('DD/MM/YYYY')).add('hours', 23).add('minutes', 59).add('seconds', 59);

    var usersToday = Meteor.users.find({createdAt: {
      $gt: today,
      $lt: endToday
    }}).count();

    return usersToday;
  },
  lastUsers: function () {
    var lastUsers = Meteor.users.find({}, {
      sort: {createdAt: -1}
    });
    var lastUsersFetch = lastUsers.fetch().slice(0, 5);
    lastUsersFetch = _.map(lastUsersFetch, function (u) {
      _.extend(u, {
        createdAt_str: moment(u.createdAt).format('DD/MM/YYYY HH:mm:ss')
      });
      return u;
    });
    return lastUsersFetch;
  },
  lastEvents: function () {
    var lastEvents = Events.find({}, {
      sort: {creation_date: -1}
    });
    var lastEventsFetch = lastEvents.fetch().slice(0, 5);
    lastEventsFetch = _.map(lastEventsFetch, function (e) {
      var user = Meteor.users.findOne({_id: e.author_id});
      var place = Places.findOne({_id: e.place_id});
      var city = place ? Cities.findOne({_id: place.city_id}) : { name: 'Cidada não encontrada' };

      _.extend(e, {
        author: user ? user.name : 'Sem autor',
        place: place,
        city: city
      });
      return e;
    });
    return lastEventsFetch;
  }
});