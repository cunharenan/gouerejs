Template.admin_events_list_item.helpers({
	labelClass: function () {
		if(this.status == EVENTS_STATUS.enabled) {
			return 'label-success';
		}
		else {
			return 'label-danger';
		}
	},
	labelStatus: function () {
		if(this.status == EVENTS_STATUS.enabled) {
			return i18n.t('Ativo');
		}
		else {
			return i18n.t('Inativo');
		}
	},
  isActive: function () {
    return this.status == EVENTS_STATUS.enabled;
  }
});

Template.admin_events_list_item.events({
	'click .btn-active': function (e) {
		var id = this._id;
		Meteor.call('events_enable', id, function (error, res) {
			if(error) {
        throwError(error.reason);
        clearSeenErrors();
			}
		});
	},
	'click .btn-inactive': function (e) {
		var id = this._id;
		Meteor.call('events_disable', id, function (error, res) {
			if(error) {
        throwError(error.reason);
        clearSeenErrors();
			}
		});
	},
	'click .btn-edit': function (e) {
		var id = this._id;
		Router.go('admin_events_edit', {_id: this._id});
	},
	'click .btn-change-picture': function (e) {
		var id = this._id;
		Router.go('admin_events_change_picture', {_id: this._id});
	}
});