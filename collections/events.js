Events = new Meteor.Collection('events');

Events.allow({
	insert: isAdminById,
	update: isAdminById,
	remove: isAdminById
});

//events avaliations base
var events_avaliations_base = {
	place: {sum: 0, total: 0, avg: 0, users: []},
	attractions: {sum: 0, total: 0, avg: 0, users: []},
	parking: {sum: 0, total: 0, avg: 0, users: []},
	organization: {sum: 0, total: 0, avg: 0, users: []},
	people: {sum: 0, total: 0, avg: 0, users: []}
};

Meteor.methods({
	
	//Insert new event
	'events_new': function (data) {
		var name = data.name,
			start_date = data.start_date,
			start_time = data.start_time,
			place_id = data.place_id,
			description = data.description,
			user = Meteor.user();

		if(!user || !user.isAdmin)
			throw new Meteor.Error(601, 'Para criar um novo evento é preciso ser um administrador!');

		if(!name || !start_date || !start_time || !place_id)
			throw new Meteor.Error(409, 'É preciso preencher todos os campos!');

		start_date = moment(start_date + ' ' + start_time, 'DD/MM/YYYY HH:mm').unix();

		var evt = {
			name: name,
			start_date: start_date,
			place_id: place_id,
			description: description,
			author_id: user._id,
			creation_date: moment().unix(),
			status: EVENTS_STATUS.enabled,
			last_change: moment().unix(),
			attractions: data.attractions || {},
			avaliations: _.clone(events_avaliations_base)
		}

		var event_id = Events.insert(evt);

		return event_id;
	},
	
	//Edit event
	'events_edit': function (id, data) {
		var evt = Events.findOne(id),
			name = data.name,
			start_date = data.start_date,
			start_time = data.start_time,
			place_id = data.place_id,
			description = data.description,
			user = Meteor.user();

		if(!user || !user.isAdmin)
			throw new Meteor.Error(601, 'Para criar um novo evento é preciso ser um administrador!');

		if(!evt)
			throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + id);

		if(!name || !start_date || !start_time || !place_id)
			throw new Meteor.Error(409, 'É preciso preencher todos os campos!');

		start_date = moment(start_date + ' ' + start_time, 'DD/MM/YYYY HH:mm').unix();

		var realAttractions = evt.attractions || {};
		var attractions = _.extend(realAttractions, data.attractions);

		var event_id = Events.update({_id: id}, {
			$set: {
				name: name,
				start_date: start_date,
				description: description,
				place_id: place_id,
				last_change: moment().unix(),
				attractions: attractions
			}
		})		

		return event_id;
	},

	//enable an event
	'events_enable': function (event_id) {
		var user = Meteor.user(),
			event = Events.findOne(event_id);

		if(!user || !user.isAdmin)
			throw new Meteor.Error(601, 'Para alterar o status de um evento é preciso ser um administrador!');

		if(!event)
			throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + event_id);

		if(event.status == EVENTS_STATUS.enabled)
			throw new Meteor.Error(601, 'O evento ' + event.name + ' já se encontra ativo!');

		Events.update({_id: event_id}, {$set: {status: EVENTS_STATUS.enabled}});

		return event_id;
	},

	//disable an event
	'events_disable': function (event_id) {
		var user = Meteor.user(),
			event = Events.findOne(event_id);

		if(!user || !user.isAdmin)
			throw new Meteor.Error(601, 'Para alterar o status de um evento é preciso ser um administrador!');

		if(!event)
			throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + event_id);

		if(event.status == EVENTS_STATUS.disabled)
			throw new Meteor.Error(601, 'O evento ' + event.name + ' já se encontra inativo!');

		Events.update({_id: event_id}, {$set: {status: EVENTS_STATUS.disabled}});

		return event_id;
	},

	events_change_picture: function (event_id, picture_id) {
		var user = Meteor.user(),
			event = Events.findOne(event_id);

		if(!user || !user.isAdmin)
			throw new Meteor.Error(601, 'Para alterar o status de um evento é preciso ser um administrador.');

		if(!event)
			throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + event_id);

		if(event.status == EVENTS_STATUS.disabled)
			throw new Meteor.Error(601, 'O evento ' + event.name + ' não pode ter sua foto alterada pois se encontrada inativo.');

		var picture = EventsFS.find({_id: picture_id}).fetch()[0];
		if(picture) {
			Events.update({_id: event_id }, {$set: {
				picture_id: picture_id,
				last_change_picture: moment().unix()
			}});
			return event_id;
		}
		else {
			throw new Meteor.Error(601, 'Não foi possível armazenar a nova imagem para o evento.');
		}
	},

	'events_vote': function (event_id, criteria, rate) {
		var event = Events.findOne({_id: event_id});
		var avaible_criteria = ['place', 'attractions', 'organization', 'parking', 'people'];
		var isValidCriteria = _.find(avaible_criteria, function (c) { return c == criteria; });
		var user = Meteor.user();

		if(!event) 
			throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + event_id);

		if(!isValidCriteria)
			throw new Meteor.Erro(602, 'Não existe avaliação para o critério ' + criteria);

		event.avaliations = event.avaliations || [];
		event.avaliations[criteria] = event.avaliations[criteria] || [];
		event.avaliations[criteria]['users'] = event.avaliations[criteria]['users'] || [];
		var alreadyUsers = event.avaliations[criteria]['users'];
		
		var alreadyVoted = _.find(alreadyUsers, function (u) { return u == user._id; });
		if(alreadyVoted)
			throw new Meteor.Error(602, 'Você não pode avaliar o mesmo critério duas vezes!');

		if(rate < 0 || rate > 5)
			throw new Meteor.Error(602, 'O valor que você está tentando votar é inválido!');

		var avg = ((event.avaliations[criteria]['sum'] + rate) / (event.avaliations[criteria]['total'] + 1));

		if(avg == NaN)
			avg = 0;

		switch(criteria) {
			case 'place':
				Events.update(event_id, {
					$inc: 	{
						'avaliations.place.sum': rate,
					 	'avaliations.place.total': 1 
					 },
					$push: {
						'avaliations.place.users': user._id
					},
					$set: {
						'avaliations.place.avg': avg 
					}
			 	});
			break;

			case 'attractions':
				Events.update(event_id, {
					$inc: 	{
						'avaliations.attractions.sum': rate,
					 	'avaliations.attractions.total': 1 
					 },
					$push: {
						'avaliations.attractions.users': user._id
					},
					$set: {
						'avaliations.attractions.avg': avg 
					}
			 	});
			break;

			case 'organization':
				Events.update(event_id, {
					$inc: 	{
						'avaliations.organization.sum': rate,
					 	'avaliations.organization.total': 1 
					 },
					$push: {
						'avaliations.organization.users': user._id
					},
					$set: {
						'avaliations.organization.avg': avg 
					}
			 	});
			break;

			case 'parking':
				Events.update(event_id, {
					$inc: 	{
						'avaliations.parking.sum': rate,
					 	'avaliations.parking.total': 1 
					 },
					$push: {
						'avaliations.parking.users': user._id
					},
					$set: {
						'avaliations.parking.avg': avg 
					}
			 	});
			break;

			case 'people':
				Events.update(event_id, {
					$inc: 	{
						'avaliations.people.sum': rate,
					 	'avaliations.people.total': 1 
					 },
					$push: {
						'avaliations.people.users': user._id
					},
					$set: {
						'avaliations.people.avg': avg 
					}
			 	});
			break;
		}

		var log = {
			criteria: criteria,
			event_id: event_id,
			votedAt: moment().unix(),
			score: rate
		};
		Meteor.call('events_logs_user_voted', log);
		
		return true;
	},

	'events_reset_votes': function (event_id) {
		var event = Events.findOne({_id: event_id});
		var user = Meteor.user();

		if(!event) 
			throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + event_id);

		if(!isAdmin(user))
			throw new Meteor.Error(602, 'Apenas administradores podem resetar os votos de um evento!');

		Events.update(event_id, {
			$set: {avaliations: _.clone(events_avaliations_base)}
		});

		EventsLogs.remove({
			'data.event_id': event_id
		});

		return event_id;
	},

	'events_update_picture': function (data) {
		console.log(data);
	}

});

EVENTS_STATUS = {
	disabled: 0,
	enabled: 1
}