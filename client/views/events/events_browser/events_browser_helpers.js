Template.events_browser.helpers = {};

Template.events_browser.helpers.makeDateRange = function (type) {
	var dates = {};
	switch(type) {
		case 'today':
			dates.startDate = moment();
			dates.endDate = moment();
		break;

		case 'yesterday':
			dates.startDate = moment().subtract('days', 1);
			dates.endDate = moment().subtract('days', 1);
		break;	

		case 'tomorrow':
			dates.startDate = moment().add('days', 1);
			dates.endDate = moment().add('days', 1);
		break;	

		case 'last_week':
			dates.startDate = moment().startOf('week').subtract('days', 2);
			dates.endDate = moment().startOf('week');
		break;	

		case 'next_week':
			dates.startDate = moment().endOf('week').subtract('days', 1);
			dates.endDate = moment().endOf('week').add('days', 1);
		break;	
	}
	return dates;
}
