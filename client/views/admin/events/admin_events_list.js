Template.admin_events_list.rendered = function () {
  var self = this,
    form = $(self.find('form'));

  //initialize tooltips
  $('[data-rel="tooltip"]').tooltip();

  //--- autocomplete for cities ---//
  $(this.find('[name="city"]')).autocomplete({
    source: function( request, response ) {
      Meteor.call('cities_list', request.term, function (err, res) {
        response( $.map( res, function( item ) {
          var show = item.name;
          return {
            label: show,
            value: show,
            id: item._id
          }
        }));
      });
    },
    change: function (event, ui) {
      if(ui.item) {
        $(self.find('[name="city_id"]')).val(ui.item.id);
        Session.set('places_query', {city_id: ui.item.id});
      }
      else {
        $(self.find('[name="city_id"]')).val(null);
      }
    }
  });

}

Template.admin_events_list.events({
  'submit form': function (e) {
    e.preventDefault();
    var form = $(e.currentTarget);
    var dataArray = form.serializeArray();
    var data = {};
    _.each(dataArray, function (o) {
      if(o.value && o.value !== '')
        data[o.name] = o.value;
    });

    if(!data.inactive) {
      data.status = EVENTS_STATUS.enabled;
    }

    Session.set('events_query', data);
  }
});

Template.admin_events_list.helpers({
	events: function () {
		var events = Events.find({}, {sort: {start_date: -1}}).fetch();
		var places = Places.find({_id: {$in: _.pluck(events, 'place_id') }}).fetch();
		var users = Meteor.users.find({_id: {$in: _.pluck(events, 'author_id') }}).fetch();

		//add places in events
		_.each(events, function(e) {
			var place = _.find(places, function (p) {
				return p._id == e.place_id;
			});
			e.place = place;
		});

		//add users in events
		_.each(events, function(e) {
			var user = _.find(users, function (u) {
				return u._id == e.author_id;
			});
			e.user = user;
		});

		return events;
	}
});