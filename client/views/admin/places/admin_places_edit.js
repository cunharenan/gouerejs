Template.admin_places_edit.rendered = function () {
  var self = this;

  //--- init map ---//
  $.proxy(Template.admin_places_edit.initMap, this)();

  //--- autocomplete ---//
  $(this.find('[name="city"]')).autocomplete({
    source: function( request, response ) {
      Meteor.call('cities_list', request.term, function (err, res) {
        response( $.map( res, function( item ) {
          var show = item.name;
          return {
            label: show,
            value: show,
            id: item._id,
            latitude: item.latitude,
            longitude: item.longitude
          }
        }));
      });
    },
    change: function (event, ui) {
      $(self.find('[name="city_id"]')).val(ui.item.id);
      if(ui.item.latitude && ui.item.longitude) {
        console.log(ui.item);
        Template.admin_places_edit.map.setView([ui.item.latitude, ui.item.longitude], 13);
      }
    }
  });
}

Template.admin_places_edit.events({
  'submit form': function (e) {
    e.preventDefault();
    var form = $(e.currentTarget);
    var dataArray = form.serializeArray();
    var data = {};
    _.each(dataArray, function (o) {
      if(o.value && o.value !== '')
        data[o.name] = o.value;
    });
    var id = this._id;

    Meteor.call('places_edit', id, data, function (error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
      else {
        Router.go('/admin_index');
      }
    });
  },
});

Template.admin_places_edit.initMap = function () {
  var map_id = 'map-box',
    self = this,
    container = L.DomUtil.get(map_id),
    loaded = (container && container._leaflet);

    console.log(this.firstNode);

  if(container && !loaded) { 
    var map = L.map('map-box').setView([-28.9333, -49.4833], 13);   
    L.tileLayer('http://{s}.maptile.lbs.ovi.com/maptiler/v2/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?token={devID}&app_id={appID}', {
        attribution: 'Map &copy; <a href="http://developer.here.com">Nokia</a>, Data &copy; NAVTEQ 2012',
        subdomains: '1234',
        devID: 'xyz',
        appID: 'abc'
    }).addTo(map);
    Template.admin_places_edit.map = map;

    Template.admin_places_edit.map.on('click', function (e) {
      if(!Template.admin_places_edit.marker) {
        Template.admin_places_edit.marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(Template.admin_places_edit.map);
      }
      else {
        Template.admin_places_edit.marker.setLatLng([e.latlng.lat, e.latlng.lng]);
      }

      $(self.find('[name="latitude"]')).val(e.latlng.lat);
      $(self.find('[name="longitude"]')).val(e.latlng.lng);
    });
  }
}

Template.admin_places_edit.helpers({
  place: function () {
    var place = Places.findOne(this.place_id);
    return place;
  },
  city: function () {
    var city = Cities.findOne(this.city_id);
    return city;
  }
});