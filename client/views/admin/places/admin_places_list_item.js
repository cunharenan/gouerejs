Template.admin_places_list_item.helpers({
  labelClass: function () {
    if(this.status == PLACES_STATUS.enabled) {
      return 'label-success';
    }
    else {
      return 'label-danger';
    }
  },
  labelStatus: function () {
    if(this.status == PLACES_STATUS.enabled) {
      return i18n.t('Ativo');
    }
    else {
      return i18n.t('Inativo');
    }
  },
  isActive: function () {
    return this.status == PLACES_STATUS.enabled;
  }
});

Template.admin_places_list_item.events({
  'click .btn-active': function (e) {
    var id = this._id;
    Meteor.call('places_enable', id, function (error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
    });
  },
  'click .btn-inactive': function (e) {
    var id = this._id;
    Meteor.call('places_disable', id, function (error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
    });
  },
  'click .btn-edit': function (e) {
    var id = this._id;
    Router.go('admin_places_edit', {_id: this._id});
  }
});