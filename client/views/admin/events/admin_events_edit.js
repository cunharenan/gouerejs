Template.admin_events_edit.rendered = function () {
  var self = this;
  
  //--- date picker ---//
  $(this.findAll('.date-picker')).datepicker({
    autoclose: true,
    format: 'dd/mm/yyyy'
  });

  //--- time picker ---//
  $(this.findAll('.time-picker')).timepicker({
    minuteStep: 1,
    showSeconds: false,
    showMeridian: false
  });

  //--- autocomplete ---//
  $(this.find('[name="place"]')).autocomplete({
      source: function( request, response ) {
        Meteor.call('places_list', request.term, function (err, res) {
          response( $.map( res, function( item ) {
            var show = item.name + ' - ' + item.city.name
            return {
              label: show,
              value: show,
              id: item._id
            }
          }));
        });
      },
      change: function (event, ui) {
        $(self.find('[name="place_id"]')).val(ui.item.id);
      }
    });
}

Template.admin_events_edit.helpers({
  event: function () {
    var evt = Events.findOne(this.event_id);
    var place = Places.findOne({_id: evt.place_id});
    evt.place = place;
    return evt;
  },
  startDate: function () {
    return moment.unix(this.start_date).format('DD/MM/YYYY');
  },
  startTime: function () {
    return moment.unix(this.start_date).format('HH:mm');
  },
  place: function () {
    var place = Places.findOne(this.place_id);
    return place;
  },
  isToCheck: function (attr) {
    if(attr)
      return 'checked';

    return '';
  }
});

Template.admin_events_edit.events({
  'submit form': function (e) {
    e.preventDefault();
    var form = $(e.currentTarget);
    var dataArray = form.serializeArray();
    var data = {};
    _.each(dataArray, function (o) {
      if(o.value && o.value !== '')
        data[o.name] = o.value;
    });
    var id = this._id;

    data.attractions = {
      edm: !!data['attractions.edm'],
      sertanejo: !!data['attractions.sertanejo'],
      pagode: !!data['attractions.pagode'],
      hip_hop: !!data['attractions.hip_hop'],
      rock: !!data['attractions.rock'],
      funk: !!data['attractions.funk']
    };

    Meteor.call('events_edit', id, data, function (error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
      else {
        Router.go('/admin_events_list');
      }
    });
  },

  'click .btn-cancel': function (e) {
    e.preventDefault();
    Router.go('/');
  }
});