Template.events_browser_filters.rendered = function () {
	var self = this;

	//---- prevent url change on click in tabs ----//	
	$(this.findAll('ul.nav-tabs a')).on('click', function (e) { e.preventDefault(); });

	//---- init chosen components ----//
	$(this.findAll('.chosen-select')).chosen('destroy').chosen();

	//---- init daterangepicker components ----//
	$.proxy(Template.events_browser_filters.initDaterangepicker, this)();
	
	//---- Bind events on chosen components ----//
	$(this.find('[name="city_id"]')).off('change').on('change', Template.events_browser_filters.onCityChange);
}

Template.events_browser_filters.created = function () {
	Session.set('events_query', {});
}

Template.events_browser_filters.onCityChange = function (e) {
	var city_id = $(e.target).val();
	Session.set('places_query', {city_id: city_id});
}

Template.events_browser_filters.events({
	'submit form': function (e) {
		e.preventDefault();
		var form = $(e.target);
		var dataArray = form.serializeArray();
		var query_data = {};
		_.each(dataArray, function (o) {
			if(o.value && o.value !== '')
				query_data[o.name] = o.value;
		});

    query_data.attractions = {
      edm: !!query_data['attractions.edm'],
      sertanejo: !!query_data['attractions.sertanejo'],
      pagode: !!query_data['attractions.pagode'],
      hip_hop: !!query_data['attractions.hip_hop'],
      rock: !!query_data['attractions.rock'],
      funk: !!query_data['attractions.funk']
    };

    //set the city id in cookie
    Cookie.set(CONSTS.EVENT_BROWSER_CITY, query_data.city_id);

		//force to show only enabled events
		query_data['status'] = EVENTS_STATUS.enabled;

		Session.set('events_query', query_data);
	}
});

Template.events_browser_filters.helpers({

	//states list
	states: function () {
		return States.find();
	},

	//cities list - this list is reactivity to session
	cities: function () {
		return Cities.find();
	},

	//current user state
	isMyState: function (stateId) {
		var user = Meteor.user();
		var city_id = (user && user.location) ? user.location.city_id : undefined;
		var city = city_id ? Cities.findOne(city_id) : undefined;
		var state = city ? States.findOne(city.state_id) : undefined;
		return (state && state._id == stateId);
	},

	//current user city
	isMyCity: function (cityId) {
		var user = Meteor.user();
    var cookie_city_id = Cookie.get(CONSTS.EVENT_BROWSER_CITY);
		var user_city_id = (user && user.location) ? user.location.city_id : undefined;
    var city_id = cookie_city_id ? cookie_city_id : user_city_id;
    var isMyCity = (city_id && city_id == cityId)

    if(isMyCity)
      Session.set('places_query', {city_id: city_id});

		return isMyCity;
	}

});

Template.events_browser_filters.initDaterangepicker = function () {
	var self = this;
	var el = $(this.find('.daterange_picker'));

	var options = {
		format: i18n.tc('shortDateFormat'),
		applyClass: 'btn-success btn-sm',
		cancelClass: 'btn-default btn-sm',
		locale: {
        applyLabel: i18n.t('Aplicar'),
        cancelLabel: i18n.t('Cancelar'),
        fromLabel: i18n.t('De'),
        toLabel: i18n.t('Até'),
        weekLabel: 'W',
        daysOfWeek: i18n.tc('weekDaysMin'),
        monthNames: i18n.tc('monthsSort')
    },
    events: {
    	update: function (startDate, endDate) {
    		$(self.find('[name="start_date"]')).val(moment(startDate).unix());
    		$(self.find('[name="end_date"]')).val(moment(endDate).unix());
    	}
    }
	};

	//attach the component to element
	if(el && !el.data('daterangepicker')) {
		el.daterangepicker(options);

		//prevent manually values
		el.off('keypress').on('keypress', function (e) {e.preventDefault()});
	}

	//bind events on date helpers
	$(this.findAll('.date-helper')).off('click').on('click', function (e) {
		e.preventDefault();
		var type = $(e.target).attr('data-helper');
		var dates = Template.events_browser.helpers.makeDateRange(type);
		var daterangepicker = el.data('daterangepicker');
		daterangepicker.startDate = dates.startDate;
		daterangepicker.endDate = dates.endDate;
		daterangepicker.updateInputText();
	});

	$(this.find('a[data-helper="today"]')).trigger('click');
}