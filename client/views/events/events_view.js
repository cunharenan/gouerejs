Template.events_view.ratyConfig = {
  starOn: '/images/raty/star-on.png',
  starOff: '/images/raty/star-off.png',
  starHalf: '/images/raty/star-half.png',
  hints: ['Péssimo', 'Ruim', 'Regular', 'Bom', 'Ótimo'],
  click: function (rate) {
    var criteria = $(this).attr('data-criteria');
    var event_id = Router.getData()['event_id'];
    Meteor.call('events_vote', event_id, criteria, rate, function (error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
      else {
        //Router.go('/events_view', {_id: event_id});
      }
    });
  }
}

Template.events_view.rendered = function () {
  var event = Events.findOne({_id: this.data.event_id});

  $(this.findAll('[data-rel="tooltip"]')).tooltip();

  Template.events_view.initializeRaty(event);
}

Template.events_view.initializeRaty = function (event) {
  var configBase = Template.events_view.ratyConfig;
  var configs = [];
  var user = Meteor.user();

  $('#place_rate, #attractions_rate, #parking_rate, #organization_rate, #people_rate').each(function () {
    var _this = $(this);
    var criteria = _this.attr('data-criteria');
    var alreadyVotted = _.find(event.avaliations[criteria].users, function (u) { return u == user._id; }) !== undefined;
    var avg = event.avaliations[criteria].avg || 0;

    var config = _.extend(_.clone(configBase), {
      readOnly: alreadyVotted,
      score: alreadyVotted ? avg : undefined
    });

    _this.raty(config);
  });

}

Template.events_view.helpers({
  event: function () {
    var event = Events.findOne(this.event_id);
    var picture_id = event ? event.picture_id : 0;
    var event_fs = EventsFS.findOne(picture_id);
    var place_id = event ? event.place_id : 0;
    var place = Places.findOne(place_id); 
    var city_id = place ? place.city_id : 0;
    var city = Cities.findOne(city_id);

    if(event) {
      event.picture = event_fs;
      place.city = city;
      event.place = place;
    }

    return event;
  },
  hasPicture: function () {
    return !!this.picture_name;
  },
  hasAttractions: function () {
    if(!this.attractions)
      return false;

    var has = false;
    for(var a in this.attractions) {
      if(this.attractions[a])
        has = true;
    } 

    return has;
  },
  alreadyHappened: function () {
    return this.start_date < moment().unix();
  },

  //organization
  hasOrganizationScore: function () {
    return this.avaliations.organization.total > 0; 
  },
  organizationScore: function () {
    return (this.avaliations.organization.avg).toFixed(1);
  },

  //place
  hasPlaceScore: function () {
    return this.avaliations.place.total > 0; 
  },
  placeScore: function () {
    return (this.avaliations.place.avg).toFixed(1);
  },

  //attractions
  hasAttractionsScore: function () {
    return this.avaliations.attractions.total > 0; 
  },
  attractionsScore: function () {
    return (this.avaliations.attractions.avg).toFixed(1);
  },

  //parking
  hasParkingScore: function () {
    return this.avaliations.parking.total > 0; 
  },
  parkingScore: function () {
    return (this.avaliations.parking.avg).toFixed(1);
  },

  //people
  hasPeopleScore: function () {
    return this.avaliations.people.total > 0; 
  },
  peopleScore: function () {
    return (this.avaliations.people.avg).toFixed(1);
  },

  avgScoreColor: function (avg) {
    if(avg <= 1)
      return 'badge-inverse';
    else if(avg > 1 && avg <= 2)
      return 'badge-danger';
    else if(avg > 2 && avg <= 3)
      return 'badge-warning';
    else if(avg > 3 && avg <= 4)
      return 'badge-primary';
    else
      return 'badge-success';
  },

  //avg
  avg: function () {
    var baseDivide = 0;
    var total = 0;

    for(var criteria_key in this.avaliations) {
      var criteria = this.avaliations[criteria_key];
      if(criteria.avg != 0) {
        baseDivide++;
        total += criteria.avg;
      }
    }

    return (total/baseDivide).toFixed(1);
  },
  avgLabel: function (avg) {
    if(avg <= 1)
      return 'Péssimo';
    else if(avg > 1 && avg <= 2)
      return 'Ruim';
    else if(avg > 2 && avg <= 3)
      return 'Regular';
    else if(avg > 3 && avg <= 4)
      return 'Bom';
    else
      return 'Ótimo';
  },
  avgColor: function (avg) {
    if(avg <= 1)
      return 'btn-inverse';
    else if(avg > 1 && avg <= 2)
      return 'btn-danger';
    else if(avg > 2 && avg <= 3)
      return 'btn-warning';
    else if(avg > 3 && avg <= 4)
      return 'btn-primary';
    else
      return 'btn-success';
  },

  alreadyVotted: function () {
    var avaible_criteria = ['place', 'attractions', 'organization', 'parking', 'people'];
    var votted = true;
    var user = Meteor.user();

    for(var criteria_key in this.avaliations) {
      var criteria = this.avaliations[criteria_key];
      var vote = _.find(criteria.users, function (u) { return u == user._id; });
      if(!vote)
        votted = false;
    }

    return votted;
  },
  hasVotes: function () {
    var avaible_criteria = ['place', 'attractions', 'organization', 'parking', 'people'];
    var votted = false;
    var user = Meteor.user();

    for(var criteria_key in this.avaliations) {
      var criteria = this.avaliations[criteria_key];
      if(criteria.users.length > 0)
        votted = true;
    }

    return votted; 
  },
  totalVotes: function () {
    var users = [];
    var avaible_criteria = ['place', 'attractions', 'organization', 'parking', 'people'];
    var user = Meteor.user();

    for(var criteria_key in this.avaliations) {
      var criteria = this.avaliations[criteria_key];
      users = _.union(users, criteria.users);
    }

    return users.length;
  },
  alreadyVottedMsg: function (total) {
    if(total == 0) {
      return '';
    }
    else if (total == 1) {
      return 'Apenas você avaliou este evento.';
    }
    else if (total == 2) {
      return 'Você e mais uma pessoa avaliaram este evento.';
    }
    else {
      return 'Você e mais ' + total + ' pessoas avaliaram este evento.';
    }
  },

  //logs
  logs: function () {
    var id = this._id;
    var logs = EventsLogs.find({'data.event_id': id}, {limit: 20, sort: { 'data.votedAt': -1 }}).fetch();

    _.each(logs, function (log) {
      var user = Meteor.users.findOne({_id: log.data.user_id});
      if(user) {
        log.user = user;
      }
    });

    return logs;
  },
  hasLogs: function () {
    var id = this._id;
    return EventsLogs.find({'data.event_id': id}, {limit: 20, sort: { 'data.votedAt': -1 }}).count() > 0;
  },
  criteriaFormat: function (criteria) {
    switch(criteria) {
      case 'organization': return 'a organização'; break;
      case 'place': return 'o local'; break;
      case 'attractions': return 'as atrações'; break;
      case 'parking': return 'o estacionamento'; break;
      case 'people': return 'o público'; break;
    }
  },
  scoreFormat: function (score, criteria) {
    var isMale = true;

    if(criteria == 'organization') isMale = false;

    if(isMale) {
    switch(score) {
        case 1: return 'péssimo'; break;
        case 2: return 'ruim'; break;
        case 3: return 'regular'; break;
        case 4: return 'bom'; break;
        case 5: return 'ótimo'; break;
      }
    }
    else {
      switch(score) {
        case 1: return 'péssima'; break;
        case 2: return 'ruim'; break;
        case 3: return 'regular'; break;
        case 4: return 'boa'; break;
        case 5: return 'ótima'; break;
      }
    }
  }


});

Template.events_view.events({
  'click .btn-map': function (e) {
    e.preventDefault();

    var container = L.DomUtil.get('map-box');

    if(container && container._leaflet) return;

    var map = L.map('map-box').setView([-28.9333, -49.4833], 13);   
    L.tileLayer('http://{s}.maptile.lbs.ovi.com/maptiler/v2/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?token={devID}&app_id={appID}', {
        attribution: 'Map &copy; <a href="http://developer.here.com">Nokia</a>, Data &copy; NAVTEQ 2012',
        subdomains: '1234',
        devID: 'xyz',
        appID: 'abc'
    }).addTo(map);
    Template.events_view.map = map;

    // add the marker
    var place = this.place;
    Template.events_view.marker = L.marker([place.latitude, place.longitude]).addTo(Template.events_view.map);

    // center map
    map.setView([place.latitude, place.longitude], 15); 

    var modal = $(e.target).parents('.events-view').siblings('.modal');
    modal.modal();

    modal.on('shown.bs.modal', function () {
      Template.events_view.map.invalidateSize();
    });
  }
});