Template.search.events({

});

Template.search.helpers({
  results: function () {
    return Template.search.getResults(this.q);
  },
  q: function () {
    return this.q;
  },
  isEvent: function () {
    return this.type == 'event';
  },
  isCity: function () {
    return this.type == 'city';
  },
  isPlace: function () {
    return this.type == 'place';
  }
});

Template.search.getResults = function (q) {
  var events = Events.find({name: {$regex: q, $options: 'i'}}, {sort: {start_date: -1}}).fetch();
  var places = Places.find({name: {$regex: q, $options: 'i'}}).fetch();
  var cities = Cities.find({name: {$regex: q, $options: 'i'}}).fetch();
  var results = [];

  var events_mapped = _.map(events, function (e, key) {
    var res = {
      type: 'event',
      _id: e._id,
      name: e.name,
      start_date: e.start_date,
      attractions: e.attractions
    };
    
    if(e.picture_id) {
      var picture = EventsFS.findOne({_id: e.picture_id});
      _.extend(res, {
        picture: picture
      });
    }

    var place = Places.findOne({_id: e.place_id});
    _.extend(res, {
      place: place
    });

    var city = Cities.findOne({_id: place.city_id});
    _.extend(res, {
      city: city
    });

    return res;

  });
  results = _.union(results, events_mapped);

   var places_mapped = _.map(places, function (p, key) {
    var res = {
      type: 'place',
      name: p.name
    };

    var city = Cities.findOne({_id: p.city_id});
    _.extend(res, {
      city: city
    });

    return res;
  });
  results = _.union(results, places_mapped);

   var cities_mapped = _.map(cities, function (c, key) {
    return {
      type: 'city',
      name: c.name
    }
  });
  results = _.union(results, cities_mapped);

  return results;
}