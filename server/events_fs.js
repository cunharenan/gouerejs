//--- setup events filesystem ---//
EventsFS.fileHandlers({
 "default1": function (options) {

    var blob = options.blob;
    var path = Meteor.require('path');
    var fs = Meteor.require('fs');
    var dir = path.resolve('.') + '/../../../../../public/images/events/';
    var filename = moment().unix() + '.png';
    var dirFile = dir + filename;
    console.log(dir);
    var callback = Meteor.bindEnvironment(function(err) {
        
      if(err) {
        throw new Meteor.Error(602, 'Não foi possível salvar a imagem!');
      }
      else {
        var event_id = options.fileRecord.metadata.event_id;
        var onSuccess = options.fileRecord.metadata.onSuccess;

        Events.update({_id: event_id}, {
          $set: {
            picture_name: filename
          }
        });

        if(onSuccess)
          onSuccess();
      }

    });

    var res = fs.writeFile(dirFile, blob, 'binary', callback);

    return null;
  }
});
