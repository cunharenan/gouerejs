EventsLogs = new Meteor.Collection('events_logs');

EventsLogs.allow({
  update: isAdminById,
  remove: isAdminById
});

Meteor.methods({

  'events_logs_user_voted': function (data) {
    var user = Meteor.user();
    var criteria = data.criteria;
    var event_id = data.event_id;
    var votedAt = data.votedAt;
    var score = data.score;
    var event = Events.findOne({_id: event_id});

    if(!event)
      throw new Meteor.Error(602, 'Não foi encontrado nenhum evento com o código ' + event_id);

    var log = {
      user_id: user._id,
      insertedAt: moment().unix(),
      type: EVENTS_LOGS_TYPES.user_voted,
      data: {
        votedAt: votedAt,
        criteria: criteria,
        score: score,
        event_id: event_id,
        user_id: user._id
      }      
    };

    return EventsLogs.insert(log);
  }

});

EVENTS_LOGS_TYPES = {
  user_voted: 1
}

