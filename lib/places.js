getPlacesQueryParams = function (q) {
  var find = {};
  var places_ids = [];

  if(!q)
    return {};

  if(!q.city_id && !q.ids)
    return {};

  if(q.city_id) {
    _.extend(find, {
      city_id: q.city_id
    });
  }
  else {
    _.extend(find, {
      city_id: {$in: q.ids}
    });
  }

  //regex search on name
  if(q.name) {
    _.extend(find, {
      name: {$regex: q.name, $options: 'i'}
    });
  }

  //status
  if(q.status) {
    _.extend(find, {
      status: q.status
    })
  }

  return find;
}