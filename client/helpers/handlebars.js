Handlebars.registerHelper('prettyDate', function(unixtime, options) {
	return prettyDate(unixtime);
});

Handlebars.registerHelper('isAdmin', function(user) {
	return user && user.isAdmin;
});