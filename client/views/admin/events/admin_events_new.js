Template.admin_events_new.rendered = function () {
	var self = this;
	
	//--- date picker ---//
	$(this.findAll('.date-picker')).datepicker({
		autoclose: true,
		format: 'dd/mm/yyyy'
	});

	//--- time picker ---//
	$(this.findAll('.time-picker')).timepicker({
		minuteStep: 1,
		showSeconds: false,
		showMeridian: false
	});

	//--- autocomplete ---//
	$(this.find('[name="place"]')).autocomplete({
      source: function( request, response ) {
      	Meteor.call('places_list', request.term, function (err, res) {
					response( $.map( res, function( item ) {
            var show = item.name + ' - ' + item.city.name
            return {
            	label: show,
            	value: show,
            	id: item._id
            }
          }));
      	});
      },
      change: function (event, ui) {
      	$(self.find('[name="place_id"]')).val(ui.item.id);
      }
    });
}

Template.admin_events_new.events({
	'submit form': function (e) {
		e.preventDefault();
		var form = $(e.currentTarget);
		var dataArray = form.serializeArray();
		var data = {};
		_.each(dataArray, function (o) {
			if(o.value && o.value !== '')
				data[o.name] = o.value;
		});

    data.attractions = {
      edm: !!data['attractions.edm'],
      sertanejo: !!data['attractions.sertanejo'],
      pagode: !!data['attractions.pagode'],
      hip_hop: !!data['attractions.hip_hop'],
      rock: !!data['attractions.rock']
    };

		Meteor.call('events_new', data, function (error, res) {
			if(error) {
        throwError(error.reason);
        clearSeenErrors();
			}
			else {
				Router.go('/admin_events_list');
			}
		});
	},

	'click .btn-cancel': function (e) {
		e.preventDefault();
		Router.go('/');
	}
});