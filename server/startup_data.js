Meteor.startup(function () {

	//set initial data
	if(Cities.find({}).count() == 0) {
		console.log('Inserting states...');
		var state_sc = States.insert({
			name: 'Santa Catarina',
			abbr: 'SC'
		});

		console.log('Inserting cities...');
		var cri_id = Cities.insert({
			name: 'Criciúma',
			latitude: -28.672627,
			longitude: -49.387456,
			state_id: state_sc
		});

		var aru_id = Cities.insert({
			name: 'Araranguá',
			state_id: state_sc,
			latitude: -28.672627,
			longitude: -49.387456
		});
	}

	if(Meteor.users.find({}).count() == 1) {
		Meteor.users.update({}, {$set: {isAdmin: true}});
	}

	var resetData = false;

	if(resetData) {

		States.remove({}); console.log('Removing states...');
		Cities.remove({}); console.log('Removing cities...');

		console.log('Inserting states...');
		var state_sc = States.insert({
			name: 'Santa Catarina',
			abbr: 'SC'
		});

		States.insert({
			name: 'Rio Grande do Sul',
			abbr: 'SC'
		});

		console.log('Inserting cities...');
		var cri_id = Cities.insert({
			name: 'Criciúma',
			latitude: -28.672627,
			longitude: -49.387456,
			state_id: state_sc
		});

		var aru_id = Cities.insert({
			name: 'Araranguá',
			state_id: state_sc,
			latitude: -28.9333,
			longitude: -49.4833
		});

		Events.remove({}); console.log('Removing events...');
		Places.remove({}); console.log('Removing places...');

		console.log('Inserting places...');
		var place_1 = Places.insert({
			name: 'Dioxxy Lounge',
			latitude: -28.672627,
			longitude: -49.387456,
			city_id: cri_id
		});

		var place_2 = Places.insert({
			name: 'Oz Music',
			latitude: -28.676731,
			longitude: -49.375536,
			city_id: cri_id
		});

		console.log('Inserting events...');
		Events.insert({
			name: 'Festa do Bara',
			start_date: moment().unix(),
			place_id: place_1
		});

		Events.insert({
			name: 'Festa do asdasdsadas Bara',
			start_date: moment().add('day', 1).unix(),
			place_id: place_2,
			status: EVENTS_STATUS.enabled
		});

		Events.insert({
			name: 'Jaburaia',
			start_date: moment().unix(),
			place_id: place_2,
			status: EVENTS_STATUS.enabled
		});

		Events.insert({
			name: 'Evento duplicado',
			start_date: moment().unix(),
			place_id: place_2,
			status: EVENTS_STATUS.enabled
		});

	}

});