//--------------------------------------------------------------------------------------------------//
//--------------------------------------- General config -------------------------------------------//
//--------------------------------------------------------------------------------------------------//
Router.configure({
		layoutTemplate: 'layout',
		loadingTemplate: 'loading',
		notFoundTemplate: 'not_found',
});


//--------------------------------------------------------------------------------------------------//
//--------------------------------------- Filters --------------------------------------------------//
//--------------------------------------------------------------------------------------------------//
var filters = {

  resetScroll: function () {
    var scrollTo = window.currentScroll || 0;
    $('body').scrollTop(scrollTo);
    $('body').css("min-height", 0);
  },

	isLoggedIn: function() {
		if (!(Meteor.loggingIn() || Meteor.user())) {
			//throwError(i18n.t('É preciso estar logado para acessar esta página!'))
			this.redirect('signin');
			this.stop(); 
		}
	},

	isLoggedOut: function() {
		if(Meteor.user()){
			this.redirect('/');
		}
	},

	isAdmin: function() {
		if(!Meteor.loggingIn() && Session.get('settingsLoaded') && !isAdmin()){
			throwError(i18n.t("Desculpe, você não possui permissão para acessar esta página!"))
			this.render('no_rights');
			this.stop(); 
		}
	},

	clearEventFilterSession: function () {
		Session.set('events_query', {});
	}

}


//--------------------------------------------------------------------------------------------------//
//--------------------------------------- Before hooks ---------------------------------------------//
//--------------------------------------------------------------------------------------------------//
Router.before(filters.isLoggedIn, {except: ['signin', 'how_works']});
Router.before(filters.isLoggedOut, {only: ['signin', 'how_works']});

//commented because i dont know why the a tag is triggering router.before filters
//Router.before(filters.clearEventFilterSession, {only: ['events_browser']});

Router.before(filters.isAdmin, {only: ['admin_index', 'admin_events_new']});


//--------------------------------------------------------------------------------------------------//
//--------------------------------------- After hooks ----------------------------------------------//
//--------------------------------------------------------------------------------------------------//
Router.after( function () {
	clearSeenErrors();
});
Router.after(filters.resetScroll, {except: []});


//--------------------------------------------------------------------------------------------------//
//--------------------------------------- Maps -----------------------------------------------------//
//--------------------------------------------------------------------------------------------------//
Router.map(function () {

		// -------------------------------------------- General --------------------------------------- //
		this.route('signin', {
			template: 'signin',
			layoutTemplate: 'layout_container',
			before: function () {
				if(Meteor.loggingIn() && Meteor.user())
					this.redirect('/');
			}
		});
		this.route('how_works', {
			template: 'how_works',
			layoutTemplate: 'layout_container'
		});

		// -------------------------------------------- Search ---------------------------------------- //
		this.route('search', {
			template: 'search',
			layoutTemplate: 'layout_container',
			waitOn: function () {
				var q = this.params.q;
				return [
					Meteor.subscribe('search', q)
				];
			},
			data: function () {
				return { 
					q: this.params.q
				}
			}
		});


		// -------------------------------------------- Events ---------------------------------------- //
		this.route('events_browser', {
				path: '/',
				waitOn: function () {
					return Meteor.subscribe('allCities');
				}
		});
		this.route('events_view/:_id', {
			template: 'events_view',
			layoutTemplate: 'layout_container',
			waitOn: function () {
				return [
					Meteor.subscribe('singleEvent', this.params._id),
					Meteor.subscribe('eventPlaceById', this.params._id),
					Meteor.subscribe('eventCityById', this.params._id),
					Meteor.subscribe('eventPictureById', this.params._id),
					Meteor.subscribe('eventsLogsById', this.params._id)
				]
			},
			data: function () {
				return { event_id: this.params._id }
			}
		});

		// -------------------------------------------- Admin ----------------------------------------- //
		//Admin index
		this.route('admin_index', {
				path: 'admin_index',
				layoutTemplate: 'layout_sidebar',
				waitOn: function () {
					return [
						Meteor.subscribe('allUsers'),
						Meteor.subscribe('allEvents'),
						Meteor.subscribe('allPlaces'),
						Meteor.subscribe('allCities')
					];
				}
		});


		// -------------------------------------------- Admin Events --------------------------------- //
		// new
		this.route('admin_events_new', {
				path: 'admin_events_new',
				layoutTemplate: 'layout_sidebar'
		});

		// list
		this.route('admin_events_list', {
			path: 'admin_events_list',
			before: function () {
				Session.set('events_query', null);
				Session.set('places_query', null);
			},
			layoutTemplate: 'layout_sidebar'
		});

		// edit
		this.route('admin_events_edit', {
			path: 'admin_events_edit/:_id',
			layoutTemplate: 'layout_sidebar',
			waitOn: function () {
				return Meteor.subscribe('singleEvent', this.params._id);
			},
			data: function () {
				return { event_id: this.params._id }
			}
		});

		// change picture
		this.route('admin_events_change_picture', {
			path: 'admin_events_change_picture/:_id',
			layoutTemplate: 'layout_sidebar',
			waitOn: function () {
				Meteor.subscribe('singleEvent', this.params._id);
			},
			data: function () {
				return { event_id: this.params._id }
			}
		});


		// -------------------------------------------- Admin Places ------------------------------------- //
		// new
		this.route('admin_places_new', {
			path: 'admin_places_new',
			layoutTemplate: 'layout_sidebar'
		});

		// list
		this.route('admin_places_list', {
			path: 'admin_places_list',
			before: function () {
				Session.set('events_query', null);
				Session.set('places_query', null);
			},
			layoutTemplate: 'layout_sidebar'
		});

		// edit
		this.route('admin_places_edit', {
			path: 'admin_places_edit/:_id',
			layoutTemplate: 'layout_sidebar',
			waitOn: function () {
				Meteor.subscribe('singlePlace', this.params._id);
			},
			data: function () {
				return { place_id: this.params._id }
			}
		});
});