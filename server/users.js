Accounts.onCreateUser(function(options, user){
	var fb_access_token = user.services.facebook.accessToken;
	var current_location = Meteor.call('fb_getUserLocation', fb_access_token);
	
	if(current_location) {
		var city = Cities.findOne({name: current_location.city});
		if(city) {
			user.location = {
				city_id: city._id
			}
		} 
	}

	//create a easy way to get user name
	user.first_name = user.services.facebook.first_name;
	user.last_name = user.services.facebook.last_name;
	user.name = user.services.facebook.name;

	//if is the first user, set it as admin
	if(Meteor.users.find({}).count() == 0) {
		user.isAdmin = true;
	}

	return user;
});