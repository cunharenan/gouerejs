Template.events_browser_map.rendered = function () {

	//---- init map ----//	
	Template.events_browser_map.initMap();

	//---- process the map ui ----//
	Template.events_browser_map.processUI();
}

Template.events_browser_map.events = function () {
	return Events.find(getEventsQueryParams(Session.get('events_query')));
}

Template.events_browser_map.count = function () {
	return Template.events_browser_map.events().count();
}

Template.events_browser_map.initMap = function() {
	var map_id = 'map-box',
		container = L.DomUtil.get(map_id),
		loaded = (container !== undefined && container._leaflet);

	if(!loaded) {
		var user = Meteor.user();
		var cookie_city_id = Cookie.get(CONSTS.EVENT_BROWSER_CITY);
		var user_city_id = (user && user.location) ? user.location.city_id : undefined;
    var city_id = cookie_city_id ? cookie_city_id : user_city_id;
    var city = Cities.findOne(city_id);
    
		var latLng = city ? [city.latitude, city.longitude] : [-28.9333, -49.4833];

		var map = L.map('map-box').setView(latLng, 13);	
		L.tileLayer('http://{s}.maptile.lbs.ovi.com/maptiler/v2/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?token={devID}&app_id={appID}', {
			attribution: 'Map &copy; <a href="http://developer.here.com">Nokia</a>, Data &copy; NAVTEQ 2012',
			subdomains: '1234',
			devID: 'xyz',
			appID: 'abc'
		}).addTo(map);
		Template.events_browser_map.map = map;
	}
}

Template.events_browser_map.processUI = function () {
	var events = Template.events_browser_map.events().fetch();
	var places_ids = _.pluck(events, 'place_id');
	var places = Places.find({_id: {$in: places_ids}}).fetch();
	var map = Template.events_browser_map.map;

	_.each(events, function(e) {
		var place = _.find(places, function (p) {
			return p._id == e.place_id;
		});
		e.place = place;
	});

	Template.events_browser_map.putMarkers(events);

	Template.events_browser_map.centerMap(places);
}

Template.events_browser_map.putMarkers = function (events) {
	var map = Template.events_browser_map.map;

	//here we need to group events by location
	var events_groups = {};
	_.each(events, function (e) {
		var place_id = e.place_id;
		if(!events_groups[place_id])
			events_groups[place_id] = [];

		events_groups[place_id].push(e);
	});

	_.each(_.keys(events_groups), function(k) {
		
		var eg = events_groups[k];
		var place = eg[0].place;

		var marker = L.marker([place.latitude, place.longitude]);
		var popupHtml = Template.events_browser_map.getPopupHTML(eg);

		marker.bindPopup(popupHtml);
		marker.addTo(map);
	});

}

Template.events_browser_map.centerMap = function (places) {
	var map = Template.events_browser_map.map;
	var latLongPairs = _.map(places, function (p) { return [p.latitude, p.longitude] });
	if(_.size(latLongPairs) > 0) {
		map.fitBounds(latLongPairs);
	}
}

Template.events_browser_map.getPopupHTML = function (eg) {
	var tplMain = $('<div/>').addClass('events-browser-popup');

	_.each(eg, function (e, index) {
		var tpl = Template.events_browser_popup();
		tpl = tpl.replace('[[name]]', e.name)
						 .replace('[[_id]]', e._id)
						 .replace('[[place_name]]', e.place.name)
					 	 .replace('[[start_date]]', prettyDate(e.start_date));

	 	 	function pushBadge(label, classCss) {
	 	 		classCss = 'badge-light';
	 	 		return '<strong class="badge ' + classCss +'">' + label + '</strong>';
	 	 	}

	 	 	e.attractions = e.attractions || {};

 	 		var attractionsHtml = '';
 	 		if(e.attractions.edm) {
 	 			attractionsHtml += pushBadge('EDM', 'badge-danger');
 	 		}

 	 		if(e.attractions.sertanejo) {
 	 			attractionsHtml += pushBadge('Sertanejo', 'badge-success');
 	 		}

 	 		if(e.attractions.pagode) {
 	 			attractionsHtml += pushBadge('Pagode', 'badge-yellow');
 	 		}

 	 		if(e.attractions.hip_hop) {
 	 			attractionsHtml += pushBadge('Hip Hop', 'badge-purple');
 	 		}

 	 		if(e.attractions.rock) {
 	 			attractionsHtml += pushBadge('Rock', 'badge-inverse');
 	 		}

 	 		if(e.attractions.funk) {
 	 			attractionsHtml += pushBadge('Funk', 'badge-info');
 	 		}
 	 		tpl = tpl.replace('[[attractions]]', attractionsHtml);

			tplMain.append(tpl);
	 	 	if(index+1 < eg.length) {
	 	 		tplMain.append('<hr/>');
	 	 	}
	 	 
	});

	return $('<div/>').append(tplMain).html();
}