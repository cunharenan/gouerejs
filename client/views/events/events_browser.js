Template.events_browser.rendered = function () {

	//---- Initialize chosen components ----//
	$(this.findAll('.chosen-select')).chosen('destroy').chosen();

	//---- Bind events on chosen components ----//
	$(this.find('[name="state"]')).off('change').on('change', Template.events_browser.onStateChange);

	//---- Initialize mapbox component ----//
	var initializeMap = $.proxy(Template.events_browser.initializeMap, this);
	initializeMap();
}

Template.events_browser.onStateChange = function (e) {
	var state_id = $(e.target).val();
	if(state_id) {
		Session.set('selected_state_id', state_id);
	}
	else {
		Session.set('selected_state_id', null);
	}
}

Template.events_browser.initializeMap = function () {
	var map_id = 'map-box',
		container = L.DomUtil.get(map_id),
		loaded = (container !== undefined && container._leaflet);

	if(!loaded) {
		var map = L.map('map-box').setView([-28.9333, -49.4833], 13);	
		// add an OpenStreetMap tile layer
		L.tileLayer('http://{s}.maptile.lbs.ovi.com/maptiler/v2/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?token={devID}&app_id={appID}', {
	attribution: 'Map &copy; <a href="http://developer.here.com">Nokia</a>, Data &copy; NAVTEQ 2012',
	subdomains: '1234',
	devID: 'xyz',
	appID: 'abc'
}).addTo(map);
	}
	
}

Template.events_browser.helpers({
	states: function () {
		return States.find();
	},
	cities: function () {
		return Cities.find();
	}
});
