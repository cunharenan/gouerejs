Template.error.errors= function(){
	return Errors.find();
}

Template.error.rendered = function () {
    var scrollTo = window.currentScroll || 0;
    $('body').scrollTop(scrollTo);
    $('body').css("min-height", 0);
}