Cities = new Meteor.Collection('cities');

Cities.allow({
	insert: isAdminById,
	update: isAdminById,
	remove: isAdminById
});

Meteor.methods({

	//list of cities for autocomplete
	'cities_list': function (term) {
		var cities = Cities.find({name: { $regex : term, $options: 'i' }}, {limit: 10}).fetch();
		return cities;
	}

});