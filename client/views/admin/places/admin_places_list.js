Template.admin_places_list.rendered = function () {
  var self = this,
    form = $(self.find('form'));

  //--- autocomplete for cities ---//
  $(this.find('[name="city"]')).autocomplete({
    source: function( request, response ) {
      Meteor.call('cities_list', request.term, function (err, res) {
        response( $.map( res, function( item ) {
          var show = item.name;
          return {
            label: show,
            value: show,
            id: item._id
          }
        }));
      });
    },
    change: function (event, ui) {
      if(ui.item) {
        $(self.find('[name="city_id"]')).val(ui.item.id);
      }
      else {
        $(self.find('[name="city_id"]')).val(null);
      }
    }
  });

}

Template.admin_places_list.events({
  'submit form': function (e) {
    e.preventDefault();
    var form = $(e.currentTarget);
    var dataArray = form.serializeArray();
    var data = {};
    _.each(dataArray, function (o) {
      if(o.value && o.value !== '')
        data[o.name] = o.value;
    });

    if(!data.inactive) {
      data.status = PLACES_STATUS.enabled;
    }

    Session.set('places_query', data);
  }
});

Template.admin_places_list.helpers({
  places: function () {
    var places = Places.find({}, {sort: {creation_date: -1}}).fetch();
    var cities = Cities.find({_id: {$in: _.pluck(places, 'city_id') }}).fetch();

    //add cities in places
    _.each(places, function(p) {
      var city = _.find(cities, function (c) {
        return c._id == p.city_id;
      });
      p.city = city;
    });

    return places;
  }
});