canView = function(user){

  if(!user)
    return false;
  
  return true;
}

canViewById = function(userId){
	return Meteor.users.find({_id: userId}).count() > 0;
}