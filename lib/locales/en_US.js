i18n.languages['en_US'] = {

	complex: {
		weekDaysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
		monthsSort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
	},

	'Sair': 'Logout'
}