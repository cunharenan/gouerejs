getSetting = function(setting, defaultValue){
	var settings = Settings.find().fetch()[0];
	if(settings){
		return settings[setting];
	}
	return typeof defaultValue === 'undefined' ? '' : defaultValue;
}
clearSeenErrors = function(){
	Errors.update({seen:true}, {$set: {show:false}}, {multi:true});
}

// ---------------------------------- Form Conversion Helper Functions -------------------------- //
formArrayToObj = function (array) {
  var data = {};
  _.each(dataArray, function (o) {
    if(o.value && o.value !== '')
      var names = o.name.split('.');
      var obj = {};
      for (var i = 0; i < names.length; i++) {
        
      };
  });
}

// ---------------------------------- String Helper Functions ----------------------------------- //
cleanUp = function(s){
  return stripHTML(s);
}
stripHTML = function(s){
  return s.replace(/<(?:.|\n)*?>/gm, '');
}
stripMarkdown = function(s){
  var converter = new Markdown.Converter();
  var html_body = converter.makeHtml(s);
  return stripHTML(html_body);
}
trimWords = function(s, numWords) {
  expString = s.split(/\s+/,numWords);
  if(expString.length >= numWords)
    return expString.join(" ")+"…";
  return s;
}