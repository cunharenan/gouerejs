Template.sidebar.events({

	'click #sidebar-collapse': function (e) {
		e.preventDefault();
		Template.sidebar.toggle();
	},

	'click a.dropdown-toggle': function (e) {
		var el = $(e.currentTarget),
			submenu = el.siblings('.submenu');
		e.preventDefault();

		submenu.slideToggle(200);
	}

});

Template.sidebar.open = function (sidebar) {
	var sidebar = (typeof(sidebar) === 'undefined') ? $('#sidebar') : sidebar,
		collapser = sidebar.find('#sidebar-collapse i');

	sidebar.removeClass('menu-min');
	collapser.removeClass(collapser.attr('data-icon-right')).addClass(collapser.attr('data-icon-left'));
}

Template.sidebar.close = function (sidebar) {
	var sidebar = (typeof(sidebar) === 'undefined') ? $('#sidebar') : sidebar,
		collapser = sidebar.find('#sidebar-collapse i');

	sidebar.addClass('menu-min');
	collapser.addClass(collapser.attr('data-icon-right')).removeClass(collapser.attr('data-icon-left'));
}

Template.sidebar.toggle = function (sidebar) {
	var sidebar = (typeof(sidebar) === 'undefined') ? $('#sidebar') : sidebar;

	if(sidebar.hasClass('menu-min'))
		Template.sidebar.open(sidebar);
	else
		Template.sidebar.close(sidebar);
}