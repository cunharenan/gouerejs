function Facebook(accessToken) {
  this.fb = Meteor.require('fbgraph');
  this.accessToken = accessToken;
  this.fb.setAccessToken(this.accessToken);
  this.options = {
    timeout: 3000,
    pool: {maxSockets: Infinity},
    headers: {connection: "keep-alive"}
  }
  this.fb.setOptions(this.options);
}

Facebook.prototype.query = function(query, method) {
  var self = this;
  var method = (typeof method === 'undefined') ? 'get' : method;
  var data = Meteor.sync(function(done) {
    self.fb[method](query, function(err, res) {
        done(null, res);
    });
  });
  return data.result;
}

Facebook.prototype.getUserData = function() {
  return this.query('/me');
}

Facebook.prototype.getUserLocation = function() {
  var fql = 'SELECT current_location FROM user WHERE uid = me()';
  var fb_result = this.query(fql, 'fql');
  var result = (fb_result && fb_result['data'] && _.isArray(fb_result['data']) && _.size(fb_result['data']) > 0) ? fb_result['data'][0]['current_location'] : undefined;

  return result;
}

Meteor.methods({
  fb_getUserData: function(accessToken) {
    accessToken = accessToken ? accessToken : Meteor.user().services.facebook.accessToken;
    var fb = new Facebook(accessToken);
    var data = fb.getUserData();
    return data;
  },
  fb_getUserLocation: function(accessToken) {
    accessToken = accessToken ? accessToken : Meteor.user().services.facebook.accessToken;
    var fb = new Facebook(accessToken);
    var data = fb.getUserLocation();
    return data;
  }
});