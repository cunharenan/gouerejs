Template.navbar.rendered = function () {
  $(this.find('.sign-in-text-facebook')).text(i18n.t('Acesse com o seu Facebook'));
}

Template.navbar.events({
	'click .btn-logout': function (e) {
		e.preventDefault();
		Meteor.logout();
	},
  'submit form': function (e) {
    var form = $(e.target);
    var q = form.find('[name="q"]');
    if(q.val().trim() == '')
      e.preventDefault();
  }
});