Template.events_browser_results.rendered = function () {
	
	//----- init scroll in the results div -----//
	//$(this.find('.scrolled')).slimScroll();
}

Template.events_browser_results.helpers({
	events: function () {
		var events = Events.find({}).fetch();
		var places = Places.find({_id: {$in: _.pluck(events, 'place_id') }}).fetch();

		_.each(events, function(e) {
			var place = _.find(places, function (p) {
				return p._id == e.place_id;
			});
			e.place = place;
		});

		return events;
	},
	count: function () {
		return Events.find({}).count();
	},
	moreThanOne: function () {
		return Events.find({}).count() > 0; 
	},
	alreadyLoaded: function () {
		if(!this.alreadyLoaded) {
			this.alreadyLoaded = true;
			return false;
		}
		else {
			return true;
		}
	},
	resultsCount: function () {
		var eCount = Events.find({}).count();
		if(eCount > 0) {
			return '(' + eCount + ' ' + (eCount > 1 ? i18n.t('eventos') : i18n.t('evento')) + ')'
		}
		else
			return '';
	},
  hasAttractions: function () {
    if(!this.attractions)
      return false;

    var has = false;
    for(var a in this.attractions) {
      if(this.attractions[a])
        has = true;
    } 

    return has;
  }
});