prettyDate = function(timestamp) {
	
	var getToday = function () {
		return moment();
	},
	getTomorrow = function () {
		return moment().add('days', 1);
	},
	getYesterday = function () {
		return moment().add('days', -1);
	},
	getLastDayOfWeek = function () {
		return moment().endOf('week');
	}

	var date = moment.unix(timestamp),
		isToday = date.isSame(getToday(), 'day'),
		isTomorrow = date.isSame(getTomorrow(), 'day'),
		isYesterday = date.isSame(getYesterday(), 'day'),
		time = date.format('HH:mm');

	if(isToday) {
		return i18n.t('hoje às') + ' ' + time;
	}
	else if(isTomorrow) {
		return i18n.t('amanhã às') + ' ' + time;
	}
	else if(isYesterday) {
		return i18n.t('ontem às') + ' ' + time;
	}
	else if (date.isAfter(getTomorrow()) && date.isBefore(getToday().add('days', 7))) {
		return date.format('DD/MM/YYYY') + ' ' + time;
	}
	else {
		return date.format('DD/MM/YYYY') + ' ' + time;
	}
}
