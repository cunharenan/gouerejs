EventsFS = new CollectionFS('events', {autopublish: false});

EventsFS.allow({
  insert: isAdminById,
  remove: isAdminById,
  update: isAdminById
});

EventsFS.filter({
    allow: {
        contentTypes: ['image/*']
    }
});