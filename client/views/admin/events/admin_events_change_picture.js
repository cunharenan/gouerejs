Template.admin_events_change_picture.rendered = function () {

}

Template.admin_events_change_picture.helpers({
  event: function () {
    var evt = Events.findOne(this.event_id);
    return evt;
  },
  hasPictureChanged: function () {
    return this.last_change_picture ? true : false;
  }
});

Template.admin_events_change_picture.events({
  'submit form': function (e) {
    e.preventDefault();
    var input = $(e.currentTarget).find('[name="picture"]');
    var files = input[0].files;
    var id = this._id;

    if(files.length == 0) {
      throwError(i18n.t('É preciso selecionar no mínimo um arquivo para enviar.'));
      return;
    }

    if(files.length > 1) {
      throwError(i18n.t('È permitido selecionar somente 1 arquivo para enviar.'));
      return;
    }

    var picture_id = EventsFS.storeFile(files[0], {
      event_id: id,
      onSuccess: function () {
        Router.go('/admin_events_list');
      }
    });

    Meteor.call('events_change_picture', id, picture_id, function(error, res) {
      if(error) {
        throwError(error.reason);
        clearSeenErrors();
      }
      else {

      }
    });
  }
});