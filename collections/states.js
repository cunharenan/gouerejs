States = new Meteor.Collection('states');

States.allow({
	insert: isAdminById,
	update: isAdminById,
	remove: isAdminById
});