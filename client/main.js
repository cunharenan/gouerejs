//set language on client-side
moment.lang('pt-br', i18n.tc('moment'));
moment.lang('pt-br');

//setup datepicker localization
$.fn.dates = {
	days: i18n.tc('moment.weekdays'),
	daysShort: i18n.tc('moment.weekdaysShort'),
	daysMin: i18n.tc('moment.weekdaysMin'),
	months: i18n.tc('moment.months'),
	monthsShort: i18n.tc('moment.monthsShort')
};

//set the image path of leaflet maps
L.Icon.Default.imagePath = '/images/leaflet';

//set to false to prevent fails on Meteor.user()
Session.set('settingsLoaded', false);

// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- General ------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Meteor.subscribe('settings', function(){
  Session.set('settingsLoaded', true);
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Users --------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Meteor.subscribe('currentUser');


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Places -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Deps.autorun(function () {
  var query = Session.get('places_query');
  Meteor.subscribe('filteredPlaces', query);
});


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Cities -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
//All cities
Meteor.subscribe('allCities');

//Cities by state
/*Meteor.autosubscribe(function () {
	Meteor.subscribe('citiesByState', Session.get('selected_state_id'));
});*/


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- States -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
Meteor.subscribe('allStates');


// -------------------------------------------------------------------------------------------------//
// -------------------------------------------- Events -------------------------------------------- //
// -------------------------------------------------------------------------------------------------//
// Subscribe to events using reactivity based on data stored in session named events_query
Meteor.autosubscribe(function () {
	Meteor.subscribe('filteredEvents', Session.get('events_query'));
})
